/* 
 * File:   main.cpp
 * Author: enovative
 *
 * Created on September 27, 2012, 4:55 PM
 */

#include <cstdlib>
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;

void onChange(int value, void* userdata) {
}

int main(int argc, char** argv) {
    int thres = 36;
    namedWindow("img");
    createTrackbar("Threshold", "img", &thres, 100, onChange);
    VideoCapture cv(CV_CAP_OPENNI);
    Mat bin,contour;
    Mat rgb, disparaty_map;
    while (!cv.grab()) {
        waitKey(33);
    }
    while ((waitKey(33)&255) != 27) {
        if (cv.grab()) {
            Mat saida;
            cv.retrieve(rgb, CV_CAP_OPENNI_BGR_IMAGE);
            cv.retrieve(disparaty_map, CV_CAP_OPENNI_DISPARITY_MAP);
            imshow("disparaty_map", disparaty_map);
            threshold(disparaty_map, bin, thres, 255, CV_THRESH_BINARY);

            vector<vector<Point> > contours;
            vector<Vec4i> hierarchy;
            bin.copyTo(contour);
            findContours(contour, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
            rgb.copyTo(saida,bin);
            vector<vector<Point> > contours_poly(contours.size());
            
            vector< vector<int> > hullI(contours.size());
            vector< vector<Point> > hullP(contours.size());
            vector< vector<Vec4i> > convec(contours.size());
            for (int i = 0; i < contours.size(); i++) {
                 
                Point2f center;
                float raio;
                approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
                
                minEnclosingCircle(contours_poly[i], center, raio);
                Scalar azul(255,0,0);
                raio /= 5;
                circle(saida,center,raio, azul,-1);
                
                
                convexHull(contours_poly[i],hullI[i],true);
                convexHull(contours_poly[i],hullP[i],true);
                cout << "Pontos " << hullP[i].size() << endl;
                Scalar verde(0,255,0);
                Scalar vermelho(0,0,255);
                
                if(contours_poly[i].size() >3)
                {
                        convexityDefects(contours_poly[i],hullI[i],convec[i]);
                        for(int j = 0;j<convec[i].size();j++)
                        {
                            circle(saida,contours_poly[i][convec[i][j][2]],5,vermelho,-1);
                            circle(saida,contours_poly[i][convec[i][j][0]],5,verde,-1);
                            circle(saida,contours_poly[i][convec[i][j][1]],5,azul,-1);
                        }
                }
            }
            cout << CV_VERSION << endl;
            imshow("saida", saida);
            imshow("img", bin);
        }
    }
    return 0;
}

